from django.db import models
from django.utils import timezone


class Organization(models.Model):
    organization = models.CharField(max_length=30)
    objects = models.Manager()
def __str__(self):
    return self.organization
    

class User(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    created_date = models.DateTimeField(
            default=timezone.now)


    def __str__(self):
        return self.user

