
from django.shortcuts import render,redirect
from django.contrib.auth.models import User

from .models import Organization
from .filters import OrgFilter

from django.views.decorators.http import require_POST
from django.http import HttpResponse




def index(request):
    return render(request,'index.html')



@require_POST
def cadastrar_usuario(request):
    
    user = request.POST['user']
    em = request.POST['email']
    
    if(User.objects.filter(username=user,email=em).exists() and OrgFilter(request.POST['org'])):
        return render(request, 'index.html', {'msg': 'Erro! Já existe um usuário com o mesmo e-mail ou mesmo username'})

    passw = request.POST['pass']
    newUser = User.objects.create_user(username=user, email=em, password=passw)
    g = Organization(organization=request.POST['org'])
    newUser.save()
    g.save()
    

#implementar aqui, django rules
def logado(request):    
    return HttpResponse('Logado')
